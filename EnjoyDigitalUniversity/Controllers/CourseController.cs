﻿using System.Web.Mvc;
using EnjoyDigitalUniversity.Models;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using System.Linq;
using System.Web;

namespace EnjoyDigitalUniversity.Controllers
{
    public class CourseController : SurfaceController
    {
        public PartialViewResult Listing(string department)
        {
            var model = new CourseListingViewModel();

            var courses = CurrentPage.Children.Select(Map);

            if (!string.IsNullOrWhiteSpace(department))
            {
                courses = courses.Where(x => x.Department == department);
                model.Department = department;
            }

            model.Courses = courses.ToList();

            return PartialView(model);
        }

        public PartialViewResult Detail()
        {
            var model = Map(CurrentPage);

            return PartialView(model);
        }

        public Course Map(IPublishedContent content)
        {
            var course = new Course();

            course.Id = content.Id;
            course.Title = content.GetPropertyValue<string>("title");
            course.Description = content.GetPropertyValue<string>("description");
            course.BodyText = content.GetPropertyValue<IHtmlString>("bodyText");
            course.Department = content.GetPropertyValue<string>("department");
            course.Url = content.Url;

            return course;
        }
    }
}